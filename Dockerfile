FROM alpine:3.14.2

RUN apk add --no-cache nginx-mod-http-lua lua5.1 luarocks && \
    luarocks-5.1 install lua-resty-template

COPY html /usr/share/nginx/html
COPY nginx_config /etc/nginx

EXPOSE 80

CMD ["nginx","-g","daemon off;"]
