local template = require("resty.template")
template.render([[
<html>
<body>
    <h1>Hello World! {{username}}</h1>
</body>
</html> 
]], {
    username = ngx.var.remote_user
})